//
//  Copyright © 2021 Digital Care. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MirrorCheckSDK
FOUNDATION_EXPORT double MirrorCheckSDKVersionNumber;

//! Project version string for MirrorCheckScanner.
FOUNDATION_EXPORT const unsigned char MirrorCheckSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MirrorCheckScanner/PublicHeader.h>

#import "Tracker.h"
