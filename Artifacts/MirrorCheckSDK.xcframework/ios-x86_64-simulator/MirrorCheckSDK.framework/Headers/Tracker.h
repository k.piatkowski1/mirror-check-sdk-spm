//
//  Copyright © 2021 Digital Care. All rights reserved.
//

#import <UIKit/UIKit.h>

#pragma mark - TrackerDelegate

@protocol TrackerDelegate

- (void)update:(NSArray<NSValue *>*)contour pixelCoverage:(NSInteger)pixelCoverage image:(UIImage*)image;

@end

#pragma mark - Tracker

@interface Tracker : NSObject

@property (weak, nonatomic) id <TrackerDelegate> delegate;

+ (Tracker *)sharedInstance;

- (void)setupTargetPosition:(NSArray<NSValue *>*)expectedPositions frameSize:(CGSize)frameSize screenRatio:(CGFloat)screenRatio;
- (void)process:(UIImage *)image;

@end
